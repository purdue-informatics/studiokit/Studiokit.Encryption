<Query Kind="Statements">
  <Output>DataGrids</Output>
  <Reference Relative="bin/Debug/net8.0/StudioKit.Encryption.dll">&lt;UserProfile&gt;/dev/Work/BrightspaceQA/StudioKit.Encryption/bin/Debug/net8.0/StudioKit.Encryption.dll</Reference>
  <Namespace>StudioKit.Encryption</Namespace>
  <Namespace>System.Security.Cryptography.Pkcs</Namespace>
  <Namespace>System.Security.Cryptography.X509Certificates</Namespace>
</Query>

// DO NOT SAVE ANY SENSITIVE DATA TO THIS FILE

//var thumbprint = "875E06858714DCE47AE1555D860AA4DC5DEAA5C1"; // Circuit
//var thumbprint = "B38E0C77FC097F38ACEB5F859EBD618691A7DDC4"; // Forecast
//var thumbprint = "B94F2584198EF05E278AF333AA0123202AA09CDB"; // Hotseat
//var thumbprint = "4DBC614D3235F380BE701868337722B84E847CDD"; // Passport
//var thumbprint = "68340F643829ABCA7579B3A106D48C228985D89C"; // Pattern
//var thumbprint = "03BD40E8E1916DFB1ACA4956E12C26705BFC307F"; // Replay
//var thumbprint = "875e06858714dce47ae1555d860aa4dc5deaa5c1"; // Circuit
//var thumbprint = "872EFF64C0FB88B14B8BE429EFE4A325221D0207"; // Variate
var thumbprint = "64E9494A6B374AE0E1F7CB182FCD5DD47F376A8D"; // Variate local
//var thumbprint = "02BAFEAD8B9B7FD559C0C72836F1469FE9BA984A"; // Kaptions
//var thumbprint = "9B5D34055B47001D6FFAA01BAD51869CF95F517B"; // Brightspace QA

/*** Encrypt ***/
var settingsToEncrypt = new List<string> {
 	"Test"
};
settingsToEncrypt
	.Select(s => new {
		Value = s,
		EncryptedValue = EncryptedConfigurationManager.Encrypt(thumbprint, s)
	})
	.Dump("Encrypt Output");

/*** Decrypt ***/
// Note: this will use whatever cert it was encrypted with to decrypt
var settingsToDecrypt = new List<string> {
	"MIIBqwYJKoZIhvcNAQcDoIIBnDCCAZgCAQAxggFTMIIBTwIBADA3MCMxITAfBgNVBAMMGHZhcmlhdGVfZW5jcnlwdGVkX2NvbmZpZwIQHh6CmXF2f7ZBzx2UKvgyATANBgkqhkiG9w0BAQEFAASCAQDJsr2xalJrieeZ2ljDdMQkPzjP8mDww5wMM4781M4PpfUSQa0K8ZWmUJwIgMJOoWMUCopKWwaA3d1zGIByosOcqlsF4nkU5jtydiP9eDPfHG/2a7y8xtzZAd02oBY3jaC8Iik2vA6YP11zVF4ye3vlnEPo76U9UWNC8JSo/7y+wjEL8NP2P6kirkcHB6OCLPKmCMWvjXOonkueoj36sWs4AfkW6J22ciYtuIMyjbqSTRx0ER6mZQsTWJgTwaadXY4By9u399+gOR1rcw22RiQ9zvtG6TRiOPjvvHTvQBQz42lp0aYYbBGBftV+1GwFoAKDvWZqqYrKRhtZAW9C8B5CMDwGCSqGSIb3DQEHATAdBglghkgBZQMEASoEEDLMLLQ0BNHc71dL/1fWXGKAEAQmqVIzEaIgp5GvFmVQxEQ="
};
settingsToDecrypt
	.Select(s => EncryptedConfigurationManager.Decrypt(s))
	.Dump("Decrypt Output");
