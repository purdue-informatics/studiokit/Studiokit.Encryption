﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Concurrent;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography.Pkcs;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace StudioKit.Encryption;

/// <summary>
/// Decorator for <see cref="ConfigurationManager"/>, adding encryption and decryption of values.
/// </summary>
public static class EncryptedConfigurationManager
{
	public static IConfiguration Configuration;

	private static readonly ConcurrentDictionary<string, string> SettingValues = new();

	/// <summary>
	/// Gets a configuration setting and decrypts the value if needed.
	/// Decorates CloudConfigurationManager.GetSetting, allowing encrypted values to be decrypted. Otherwise returns the value normally.
	/// Caches final values in a static Dictionary.
	/// </summary>
	/// <param name="settingKey">The configuration setting key.</param>
	/// <returns>The configuration setting value.</returns>
	public static string GetSetting(string settingKey)
	{
		if (SettingValues.TryGetValue(settingKey, out var setting))
			return setting;
		var value = Configuration?[settingKey] ?? ConfigurationManager.AppSettings[settingKey];
		value = TryDecryptSettingValue(value);
		SettingValues.TryAdd(settingKey, value);
		return value;
	}

	/// <summary>
	/// Gets a value from a KeyValueConfigurationCollection and decrypts the value if needed.
	/// Caches final values in a static Dictionary.
	/// </summary>
	/// <param name="keyValues">The collection of keyValues.</param>
	/// <param name="settingKey">The setting key.</param>
	/// <returns>The setting value.</returns>
	public static string GetKeyValue(KeyValueConfigurationCollection keyValues, string settingKey)
	{
		if (SettingValues.TryGetValue(settingKey, out var keyValue))
			return keyValue;
		var value = keyValues.AllKeys.Contains(settingKey) ? keyValues[settingKey].Value : null;
		value = TryDecryptSettingValue(value);
		SettingValues.TryAdd(settingKey, value);
		return value;
	}

	/// <summary>
	/// Wraps the Decrypt method in a try-catch block.
	/// </summary>
	/// <param name="value">The value to try to decrypt.</param>
	/// <returns>The decrypted value if successful, otherwise the original value.</returns>
	public static string TryDecryptSettingValue(string value)
	{
		try
		{
			if (value != null)
				value = Decrypt(value);
		}
		catch
		{
			// ignore
		}

		return value;
	}

	/// <summary>
	/// Encrypt a string value using a certificate.
	/// </summary>
	/// <param name="thumbprint">The certificate thumbprint. Certificate must be installed on the local machine.</param>
	/// <param name="value">The string value to encrypt.</param>
	/// <returns>The encrypted string.</returns>
	public static string Encrypt(string thumbprint, string value)
	{
		var passwordBytes = Encoding.UTF8.GetBytes(value);
		var contentInfo = new ContentInfo(passwordBytes);
		var env = new EnvelopedCms(contentInfo);

		// Note: macOS installs certs into the login keychain when double clicking from finder, so we have to look there for encrypting
		var isMacOs = System.Runtime.InteropServices.RuntimeInformation.IsOSPlatform(System.Runtime.InteropServices.OSPlatform.OSX);
		using var store = new X509Store(StoreName.My, isMacOs ? StoreLocation.CurrentUser : StoreLocation.LocalMachine);
		store.Open(OpenFlags.ReadOnly);
		var cert = store.Certificates.Single(xc => xc.Thumbprint == thumbprint);
		env.Encrypt(new CmsRecipient(cert));
		var result = Convert.ToBase64String(env.Encode());
		return result;
	}

	/// <summary>
	/// Decrypt an encrypted string. The certificate used to encrypt the string must be installed on the local machine or on the server to function properly.
	/// </summary>
	/// <param name="value">The encrypted value</param>
	/// <returns>The decrypted value</returns>
	public static string Decrypt(string value)
	{
		var bytes = Convert.FromBase64String(value);
		var env = new EnvelopedCms();
		env.Decode(bytes);
		env.Decrypt();
		var result = Encoding.UTF8.GetString(env.ContentInfo.Content);
		return result;
	}
}
